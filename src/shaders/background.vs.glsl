#version 330
layout (location = 0) in vec3 aPos;

uniform mat4 uProjMatrix;
uniform mat4 uViewMatrix;

out vec3 localPos;

void main()
{
    localPos = aPos;

	mat4 rotView = mat4(mat3(uViewMatrix));
	vec4 clipPos = uProjMatrix * rotView * vec4(localPos, 1.0);

	gl_Position = clipPos.xyww;
}