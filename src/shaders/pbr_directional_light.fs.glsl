#version 330


in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLd;
uniform vec3 uLi;

uniform sampler2D uBaseColorTexture;
uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

uniform sampler2D uEmissiveTexture;
uniform vec3 uEmissiveFactor;

uniform float uOcclusionStrength;
uniform sampler2D uOcclusionTexture;
uniform int uApplyOcclusion;


out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
    return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

vec3 schlick_fresnel(float baseSchlickFactor, vec3 f0) 
{
    float schlickFactor = baseSchlickFactor * baseSchlickFactor;
    schlickFactor *= schlickFactor;
    schlickFactor *= baseSchlickFactor; // power 5;

    return f0 + (1 - f0) * schlickFactor;
}

void main()
{
    vec3 N = normalize(vViewSpaceNormal);
    vec3 V = normalize(-vViewSpacePosition);
    vec3 L = uLd;
    vec3 H = normalize(L+V);
    const float black = 0.;

    vec4 baseColorFromTexture =
        SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
    vec4 baseColor = baseColorFromTexture*uBaseColorFactor;

    vec4 metallicRoughnessFromTexture = texture(uMetallicRoughnessTexture,vTexCoords);

    vec3 emissive = texture(uEmissiveTexture,vTexCoords).rgb * uEmissiveFactor;

    vec3 metallic = vec3(uMetallicFactor * metallicRoughnessFromTexture.b);
    float roughness = uRoughnessFactor * metallicRoughnessFromTexture.g;

// ------------------ DOTS --------------------------- //
    float VdotH = clamp(dot(V, H), 0., 1.);
    float NdotL = clamp(dot(N, L), 0., 1.);
    float NdotV = clamp(dot(N, V), 0., 1.);
    float NdotH = clamp(dot(N, H), 0., 1.);
//---------------------------------------------------- //
    float a = roughness * roughness;
    float a2 = a * a;

    vec3 c_diff = mix(baseColor.rgb, vec3(black), metallic);
    
    vec3 f0 = mix(vec3(0.04), baseColor.rgb, metallic);

    vec3 F = schlick_fresnel(1-NdotV, f0);
    
    float visDenominator = 
        NdotL + sqrt(a2 + (1 - a2) * (NdotL * NdotL)) +
        NdotV + sqrt(a2 + (1 - a2) * (NdotV * NdotV));
    float Vis = visDenominator > 0. ? 0.5 / visDenominator : 0.0;

    float dDenom = 
        M_PI * ((NdotH * NdotH) * (a2 - 1) + 1) * 
        ((NdotH * NdotH) * (a2 - 1) + 1);
    float D = a2 / dDenom;

    vec3 f_specular = F * Vis * D;

    vec3 diffuse = c_diff * M_1_PI;

    
    vec3 f_diffuse = (1. -F) * diffuse;

    vec3 color = (f_diffuse + f_specular) * uLi * NdotL;

    if (1 == uApplyOcclusion) {
        float ao = texture2D(uOcclusionTexture, vTexCoords).r;
        color = mix(color, color * ao, uOcclusionStrength);
    }


    fColor = LINEARtoSRGB(color + emissive);
}