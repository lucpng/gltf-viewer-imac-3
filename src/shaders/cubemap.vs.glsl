#version 330

layout(location = 0) in vec3 aPosition;

out vec3 LocalPosition;

uniform mat4 uViewMatrix;
uniform mat4 uProjMatrix;

void main()
{
    LocalPosition = aPosition;
    gl_Position =  uProjMatrix * uViewMatrix * vec4(LocalPosition, 1.0);
}