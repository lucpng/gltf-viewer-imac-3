#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"
#include "utils/cameras.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>
#include "stb_image.h"

unsigned int ENVMAP_RES = 1024;

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto panoToCubemap = compileProgram({m_ShadersRootPath / m_vertexCubeMapShader, 
      m_ShadersRootPath / m_fragmentPanoToCubeMapShader});
  const auto irradianceConvolutionMap = compileProgram({m_ShadersRootPath / m_vertexCubeMapShader, 
      m_ShadersRootPath / m_fragmentIrradianceConvolutionShader});
  const auto backgroundGlslProgram = compileProgram({m_ShadersRootPath / m_vertexBackgroundShader,
  m_ShadersRootPath / m_fragmentBackgroundShader});
  const auto prefilterShader = compileProgram({m_ShadersRootPath / m_vertexCubeMapShader,
  m_ShadersRootPath / m_fragmentPrefilterShader});
  const auto brdfShader = compileProgram({m_ShadersRootPath / m_vertexBRDFShader,
  m_ShadersRootPath / m_fragmentBRDFShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto uLiLocation = 
      glGetUniformLocation(glslProgram.glId(), "uLi");
  const auto uLdLocation = 
      glGetUniformLocation(glslProgram.glId(), "uLd");
  const auto uBaseColorTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  const auto uMetallicRoughnessTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto uMetallicFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");

  const auto uEmissiveTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");

  const auto uOcclusionTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrengthLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto uApplyOcclusionLocation =
      glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");

  const auto uIrradianceCubeMapLocation =
      glGetUniformLocation(glslProgram.glId(), "uIrradianceCubeMap");
      const auto uPrefilterMapLocation =
      glGetUniformLocation(glslProgram.glId(), "uPrefilterMap");
      const auto uBrdfLUTLocation =
      glGetUniformLocation(glslProgram.glId(), "uBrdfLUT");
  const auto uUseIBLLocation =
      glGetUniformLocation(glslProgram.glId(), "uUseIBL");

  const auto uBackgroundViewMatrixLocation =
    glGetUniformLocation(backgroundGlslProgram.glId(), "uViewMatrix");
  const auto uBackgroundProjMatrixLocation =
    glGetUniformLocation(backgroundGlslProgram.glId(), "uProjMatrix");
  const auto uEnvMapLocation =
    glGetUniformLocation(backgroundGlslProgram.glId(), "uEnvMap");


  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(1, 1, 1);
  bool lightFromCamera = false;
  bool applyOcclusion = true;
  bool useIBL = true;
  bool rotateModel = false;
  static const char * bgModes[]{"None", "CubeMap", "IrradianceMap"};
  static int selectedBgMode = 1;


  tinygltf::Model model;

  if (!loadGltfFile(model)) {
    return -1;
  }

  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);

  // Build projection matrix
  const auto diag = bboxMax - bboxMin;
  auto maxDistance = glm::length(diag) > 0 ? glm::length(diag) : 100.0f;

  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.7f * maxDistance);

  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  }
  else{
    const auto bboxCenter = 0.5f * (bboxMax + bboxMin);
    const auto up = glm::vec3(0,1,0);
    const auto eye = diag.z > 0 ? bboxCenter + diag : bboxCenter + 2.f * glm::cross(diag, up);
    cameraController->setCamera(Camera{eye, bboxCenter, up}); 
  }

  const auto textureObjects = createTextureObjects(model);

  float pixelWhite[] = {1,1,1};

  GLuint whiteTex;
  glGenTextures(1, &whiteTex);

  // Bind texture object to target GL_TEXTURE_2D:
  glBindTexture(GL_TEXTURE_2D, whiteTex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGB, GL_FLOAT, pixelWhite);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  const auto vbo = createBufferObjects(model);

  std::vector<VaoRange> meshIndexToVaoRange;
  const auto vaos = createVertexArrayObjects(model, vbo, meshIndexToVaoRange);

  //Load Panorama
  const auto envTextures = setupCubeMaps(panoToCubemap, irradianceConvolutionMap, prefilterShader, brdfShader);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL); // set depth function to less than AND equal for skybox depth trick.

  glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

  backgroundGlslProgram.use();
  glUniform1i(uEnvMapLocation, 0);
  glUniformMatrix4fv(uBackgroundProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(projMatrix));
  
  glslProgram.use();
  //Lambda function to bind a material
  const auto bindMaterial = [&](const auto materialIndex) {
    
    if(materialIndex >= 0){
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

      // ____________________________SEND uBaseColorFactor uniform ________________________________________

      if (uBaseColorFactorLocation >= 0) {
        glUniform4f(uBaseColorFactorLocation,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }
      else
      {
        std::cerr << "Uniform uBaseColorFactorLocation not found" << std::endl;
      }

      // ____________________________SEND uBaseColorTexture uniform ________________________________________

      if (uBaseColorTextureLocation >= 0) {
        auto textureObject = whiteTex;
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uBaseColorTextureLocation, 0);
      }
      else{
        std::cerr << "Uniform uBaseColorTexture not found" << std::endl;
      }

      // ____________________________SEND uMetallicFactor uniform ________________________________________

      if (uMetallicFactorLocation >= 0) {
        glUniform1f(
            uMetallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
      }
      else{
        std::cerr << "Uniform uMetallicFactor not found" << std::endl;
      }

      // ____________________________SEND uRoughnessFactor uniform ________________________________________

      if (uRoughnessFactorLocation >= 0) {
        glUniform1f(
            uRoughnessFactorLocation, (float)pbrMetallicRoughness.roughnessFactor);
      }
      else{
        std::cerr << "Uniform uRoughnessFactor not found" << std::endl;
      }

      // ____________________________SEND uMetallicRoughnessTexture uniform ________________________________________


      if (uMetallicRoughnessTextureLocation >= 0) {
        auto textureObject = 0u;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }
      else{
        std::cerr << "Uniform uMetallicRoughnessTexture not found" << std::endl;
      }

      // ____________________________SEND uEmissiveFactor uniform ________________________________________

      if (uEmissiveFactorLocation >= 0) {
        glUniform3f(
            uEmissiveFactorLocation, (float)material.emissiveFactor[0], 
              (float)material.emissiveFactor[1], 
              (float)material.emissiveFactor[2]);
      }
      else{
        std::cerr << "Uniform uRoughnessFactor not found" << std::endl;
      }

      // ____________________________SEND uEmissiveTexture uniform ________________________________________

      if (uEmissiveTextureLocation >= 0) {
        auto textureObject = 0u;
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uEmissiveTextureLocation, 2);
      }

      // ____________________________SEND uOcclusionStrength uniform ________________________________________

      if (uOcclusionStrengthLocation >= 0) {
        glUniform1f(uOcclusionStrengthLocation, (float)material.occlusionTexture.strength);
      }
      else{
        std::cerr << "Uniform uOcclusionStrength not found" << std::endl;
      }

      // ____________________________SEND uOcclusionTexture uniform ________________________________________

      if (uOcclusionTextureLocation >= 0) {
        auto textureObject = whiteTex;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uOcclusionTextureLocation, 3);
      }
      else{
        std::cerr << "Uniform uOcclusionTexture not found" << std::endl;
      }
    }
    else{
      // KronosGroup convention
      if (uBaseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTex);
        glUniform1i(uBaseColorTextureLocation, 0);
      }
      if (uBaseColorFactorLocation >= 0) {
        glUniform4f(uBaseColorFactorLocation, 1, 1, 1, 1);
      }

      if (uMetallicRoughnessTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTextureLocation, 1);
      }
      if (uMetallicFactorLocation >= 0 || uRoughnessFactorLocation >= 0) {
        glUniform4f(uMetallicFactorLocation, 1, 1, 1, 1);
        glUniform4f(uRoughnessFactorLocation, 1, 1, 1, 1);
      }

      if (uEmissiveTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        
        glUniform1i(uEmissiveTextureLocation, 2);
      }

      if (uEmissiveFactorLocation >= 0) {
        glUniform4f(uEmissiveFactorLocation, 0.f, 0.f, 0.f, 0.f);
      }

      if (uOcclusionStrengthLocation >= 0) {
        glUniform1f(uOcclusionStrengthLocation, 1.f);
      }

      if (uOcclusionTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTextureLocation, 3);
        
      }
    }
      //____________________________SEND uApplyOcclusion_____________________________//
      if (uApplyOcclusionLocation >= 0) {
        glUniform1i(uApplyOcclusionLocation, applyOcclusion);
      }

      //____________________________SEND uIrradianceCubeMap_____________________________//

      if (uIrradianceCubeMapLocation >= 0) {
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_CUBE_MAP, envTextures[1]);
        glUniform1i(uIrradianceCubeMapLocation, 4);
      }

      //____________________________SEND uPrefilterMap_____________________________//

      if (uPrefilterMapLocation >= 0) {
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_CUBE_MAP, envTextures[2]);
        glUniform1i(uPrefilterMapLocation, 5);
      }

      //____________________________SEND uBrdfLUT_____________________________//

      if (uBrdfLUTLocation >= 0) {
        glActiveTexture(GL_TEXTURE6);
        glBindTexture(GL_TEXTURE_2D, envTextures[3]);
        glUniform1i(uBrdfLUTLocation, 6);
      }

      //____________________________SEND uUseIBL_____________________________//
      if (uUseIBLLocation >= 0) {
        glUniform1i(uUseIBLLocation, useIBL);
      }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera, const float seconds) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();
    
    const float rotationAngle = (seconds * 0.1f);
    
    
    if (uLdLocation >= 0) {
      if(lightFromCamera){
        glUniform3f(uLdLocation, 0,0, 1);
      }else{
      const auto lightDirectionInViewSpace =
          glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
          glUniform3f(uLdLocation, lightDirectionInViewSpace[0],
          lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);
      }
    }

    if (uLiLocation >= 0) {
      glUniform3f(uLiLocation, lightIntensity[0], lightIntensity[1],
          lightIntensity[2]);
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          
          const auto &node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if (node.mesh >= 0) {
            const auto mvMatrix = viewMatrix * modelMatrix;

            const auto mvpMatrix = projMatrix * mvMatrix;

            const auto normalMatrix = glm::transpose(glm::inverse(mvMatrix));

            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvpMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

            const auto &currentMesh = model.meshes[node.mesh];
            const auto &vaoRange = meshIndexToVaoRange[node.mesh];

            for (size_t primIdx = 0; primIdx < currentMesh.primitives.size(); ++primIdx) {
            
              const auto vao = vaos[vaoRange.begin + primIdx];
              
              const auto &primitive = currentMesh.primitives[primIdx];
              bindMaterial(primitive.material);
              glBindVertexArray(vao);

              if (primitive.indices >= 0) {

                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;

                
                glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const void *)byteOffset);
              } 
              else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];

                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }

          for (const auto childIdx : node.children) {
            drawNode(node.children[childIdx], modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      
      for (const auto nodeIdx : model.scenes[model.defaultScene].nodes) {
        drawNode(nodeIdx, glm::rotate(glm::mat4(1), rotateModel ? rotationAngle : 0, glm::vec3(0,1,0)));
      }
    }
  };

  const auto drawEnvMap = [&](const Camera &camera) {
    glUniformMatrix4fv(uBackgroundViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(camera.getViewMatrix()));
    glActiveTexture(GL_TEXTURE0);
    if(selectedBgMode == 1 ){
      glBindTexture(GL_TEXTURE_CUBE_MAP, envTextures[0]); // Environnement Map Texture
    }
    else if(selectedBgMode == 2){
      glBindTexture(GL_TEXTURE_CUBE_MAP, envTextures[1]); // Irradiance Map Texture
    }
    //glBindTexture(GL_TEXTURE_CUBE_MAP, envTextures[2]); // Prefilter Map Texture
    renderCube();
  };

  if (!m_OutputPath.empty()) {
    const auto numComponents = 3;
    std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * numComponents);
    renderToImage(m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data(), [&]() {
      const auto camera = cameraController->getCamera();
      drawScene(camera, 0);});
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data());
    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0;
  }

  
  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();

    glslProgram.use();
    drawScene(camera, seconds);
    
    if(selectedBgMode > 0){
      backgroundGlslProgram.use();
      drawEnvMap(camera);
    }    

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        static int cameraControllerMode = 0;
        const bool cameraControllerModeChanged = (ImGui::RadioButton("TrackBall", &cameraControllerMode, 0) ||
        ImGui::RadioButton("FirstPerson", &cameraControllerMode, 1));

        if (cameraControllerModeChanged) {
          const auto currentCamera = cameraController->getCamera();
          if(cameraControllerMode == 0){
            cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.7f * maxDistance);
          }
          else{
            cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.7f * maxDistance);
          }

          cameraController->setCamera(currentCamera);
        }
      }
      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        static float lightTheta = 0.f;
        static float lightPhi = 0.f;

        ImGui::Checkbox("light from camera", &lightFromCamera);
        ImGui::Checkbox("apply occlusion", &applyOcclusion);
        ImGui::Checkbox("rotate model with time", &rotateModel);
        ImGui::Checkbox("use Image Based Lighting", &useIBL);
        ImGui::Combo("Background", &selectedBgMode, bgModes, IM_ARRAYSIZE(bgModes));

        if (ImGui::SliderFloat("theta", &lightTheta, 0, glm::pi<float>()) ||
            ImGui::SliderFloat("phi", &lightPhi, 0, 2.f * glm::pi<float>())) {
          const auto sinPhi = glm::sin(lightPhi);
          const auto cosPhi = glm::cos(lightPhi);
          const auto sinTheta = glm::sin(lightTheta);
          const auto cosTheta = glm::cos(lightTheta);
          lightDirection =
              glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
        }

        static glm::vec3 lightColor(1.f, 1.f, 1.f);
        static float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("color", (float *)&lightColor) ||
            ImGui::InputFloat("intensity", &lightIntensityFactor)) {
          lightIntensity = lightColor * lightIntensityFactor;
        }

      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }
  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}

bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{

  std::cout << "loading GLTF file ... " << std::endl;

  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;
  std::string filePath = m_gltfFilePath.string();
  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, filePath);

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return false;
  }

  std::cout << "Successfully load : " << filePath << std::endl;
  return true;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{

  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);

  glGenBuffers(GLsizei(model.buffers.size()), bufferObjects.data());

  for (size_t bufferIdx = 0; bufferIdx < model.buffers.size(); ++bufferIdx) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[bufferIdx].data.size(),
        model.buffers[bufferIdx].data.data(), 0);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
  std::vector<GLuint> vertexArrayObjects;

  const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
  const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
  const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

  for (size_t meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx) {
    const auto &mesh = model.meshes[meshIdx];

    const auto vaoOffset = vertexArrayObjects.size();

    vertexArrayObjects.resize(vaoOffset + mesh.primitives.size());
    meshIndexToVaoRange.push_back(
        VaoRange{GLsizei(vaoOffset), GLsizei(mesh.primitives.size())});

    glGenVertexArrays(
        GLsizei(mesh.primitives.size()), &vertexArrayObjects[vaoOffset]);

    for (size_t pIdx = 0; pIdx < mesh.primitives.size(); ++pIdx) {
      const auto vao = vertexArrayObjects[vaoOffset + pIdx];
      const auto primitive = mesh.primitives[pIdx];

      glBindVertexArray(vao);
      {
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      {
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      {
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != end(primitive.attributes)) {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          const auto bufferObject = bufferObjects[bufferIdx];

          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)byteOffset);
        }
      }
      if (primitive.indices >= 0) {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }
    }
  }
  glBindVertexArray(0);

  std::cout << "Number of VAOs: " << vertexArrayObjects.size() << std::endl;
  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const
{

  std::vector<GLuint> texObjects(model.textures.size(), 0);

  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  // Generate the texture object:
  glGenTextures(GLsizei(model.textures.size()), texObjects.data());

  for(size_t texIdx = 0; texIdx < model.textures.size(); ++texIdx){
    const auto &texture = model.textures[texIdx];
    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    assert(texture.source >= 0); // ensure a source image is present
    const auto &image = model.images[texture.source];

    

    // Bind texture object to target GL_TEXTURE_2D:
    glBindTexture(GL_TEXTURE_2D, texObjects[texIdx]);
    // Set image data:
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
            GL_RGBA, image.pixel_type, image.image.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, 
      sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
      sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
      sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
      sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
      sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
    
  }
  glBindTexture(GL_TEXTURE_2D, 0);

  return texObjects;
}

std::vector<GLuint> ViewerApplication::setupCubeMaps(const GLProgram &panoToCubeMapShader, const GLProgram &irradianceShader, const GLProgram &prefilterShader, const GLProgram &brdfShader)
{
    std::vector<GLuint> envTextures;

    const auto panoProjMatrixLocation =
      glGetUniformLocation(panoToCubeMapShader.glId(), "uProjMatrix");
    const auto panoViewMatrixLocation =
      glGetUniformLocation(panoToCubeMapShader.glId(), "uViewMatrix");
    const auto panoMapLocation =
      glGetUniformLocation(panoToCubeMapShader.glId(), "uPanoMap");
      

    const auto irraProjMatrixLocation =
      glGetUniformLocation(irradianceShader.glId(), "uProjMatrix");
    const auto irraViewMatrixLocation =
      glGetUniformLocation(irradianceShader.glId(), "uViewMatrix");
    const auto irraEnvMapLocation =
      glGetUniformLocation(irradianceShader.glId(), "uEnvMap");

    const auto prefilterProjMatrixLocation =
      glGetUniformLocation(prefilterShader.glId(), "uProjMatrix");
    const auto prefilterViewMatrixLocation =
      glGetUniformLocation(prefilterShader.glId(), "uViewMatrix");
    const auto prefilterEnvMapLocation =
      glGetUniformLocation(prefilterShader.glId(), "uEnvMap");
    const auto prefilterRoughnessLocation =
      glGetUniformLocation(prefilterShader.glId(), "uRoughness");

    unsigned int captureFBO;
    unsigned int captureRBO;
    glGenFramebuffers(1, &captureFBO);
    glGenRenderbuffers(1, &captureRBO);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, ENVMAP_RES, ENVMAP_RES);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);

    const auto hdrTexture = loadHdrTexture();

    // pbr: setup cubemap to render to and attach to framebuffer
    // ---------------------------------------------------------
    unsigned int envCubemap;
    glGenTextures(1, &envCubemap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, envCubemap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, ENVMAP_RES, ENVMAP_RES, 0, GL_RGB, GL_FLOAT, nullptr);
    } 
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // pbr: set up projection and view matrices for capturing data onto the 6 cubemap face directions
    // ----------------------------------------------------------------------------------------------
    glm::mat4 captureProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 10.0f);
    glm::mat4 captureViews[] =
    {
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  1.0f,  0.0f), glm::vec3(0.0f,  0.0f,  1.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f,  0.0f), glm::vec3(0.0f,  0.0f, -1.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  0.0f,  1.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  0.0f, -1.0f), glm::vec3(0.0f, -1.0f,  0.0f))
    };

    // pbr: convert HDR equirectangular environment map to cubemap equivalent
    // ----------------------------------------------------------------------
    panoToCubeMapShader.use();
    if (panoMapLocation >= 0) {
      glUniform1i(panoMapLocation, 0);
    }
    else{
      std::cerr << "Uniform uPanoMap not found" << std::endl;
    }

    if (panoProjMatrixLocation >= 0) {
       glUniformMatrix4fv(panoProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(captureProjection));
    }
    else{
      std::cerr << "Uniform uProjMatrix not found" << std::endl;
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, hdrTexture);

    glViewport(0, 0, ENVMAP_RES, ENVMAP_RES); // don't forget to configure the viewport to the capture dimensions.
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glUniformMatrix4fv(panoViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(captureViews[i]));
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, envCubemap, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        renderCube();
    }
    envTextures.push_back(envCubemap);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // pbr: create an irradiance cubemap, and re-scale capture FBO to irradiance scale.
    // --------------------------------------------------------------------------------
    unsigned int irradianceMap;
    glGenTextures(1, &irradianceMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceMap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 64, 64, 0, GL_RGB, GL_FLOAT, nullptr);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 64, 64);

    // pbr: solve diffuse integral by convolution to create an irradiance (cube)map.
    // -----------------------------------------------------------------------------
    irradianceShader.use();
    if (irraEnvMapLocation >= 0) {
      glUniform1i(irraEnvMapLocation, 0);
    }
    else{
      std::cerr << "Uniform uEnvMap not found" << std::endl;
    }
    if (irraProjMatrixLocation >= 0) {
       glUniformMatrix4fv(irraProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(captureProjection));
    }
    else{
      std::cerr << "Uniform uProjMatrix not found" << std::endl;
    }
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, envCubemap);

    glViewport(0, 0, 64, 64); // don't forget to configure the viewport to the capture dimensions.
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glUniformMatrix4fv(irraViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(captureViews[i]));
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, irradianceMap, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        renderCube();
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    envTextures.push_back(irradianceMap);

    //  pbr : generate prefilterMap;
    
    unsigned int prefilterMap;
    glGenTextures(1, &prefilterMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, prefilterMap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 128, 128, 0, GL_RGB, GL_FLOAT, nullptr);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); 
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    prefilterShader.use();
    if (prefilterEnvMapLocation >= 0) {
      glUniform1i(prefilterEnvMapLocation, 0);
    }
    else{
      std::cerr << "Uniform uEnvMap not found" << std::endl;
    }
    if (prefilterProjMatrixLocation >= 0) {
       glUniformMatrix4fv(prefilterProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(captureProjection));
    }
    else{
      std::cerr << "Uniform uProjMatrix not found" << std::endl;
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, envCubemap);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    unsigned int maxMipLevels = 5;
    for (unsigned int mip = 0; mip < maxMipLevels; ++mip)
    {
        // reisze framebuffer according to mip-level size.
        unsigned int mipWidth = static_cast<unsigned int>(128 * std::pow(0.5, mip));
        unsigned int mipHeight = static_cast<unsigned int>(128 * std::pow(0.5, mip));
        glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipWidth, mipHeight);
        glViewport(0, 0, mipWidth, mipHeight);

        float roughness = (float)mip / (float)(maxMipLevels - 1);

        if (prefilterRoughnessLocation >= 0) {
          glUniform1f(prefilterRoughnessLocation,roughness);
        }

        for (unsigned int i = 0; i < 6; ++i)
        {
            glUniformMatrix4fv(prefilterViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(captureViews[i]));
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, prefilterMap, mip);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            renderCube();
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    envTextures.push_back(prefilterMap);

    // pbr: generate a 2D LUT from the BRDF equations used.
    // ----------------------------------------------------
    unsigned int brdfLUTTexture;
    glGenTextures(1, &brdfLUTTexture);

    // pre-allocate enough memory for the LUT texture.
    glBindTexture(GL_TEXTURE_2D, brdfLUTTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, 512, 512, 0, GL_RG, GL_FLOAT, 0);
    // be sure to set wrapping mode to GL_CLAMP_TO_EDGE
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // then re-configure capture framebuffer object and render screen-space quad with BRDF shader.
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, brdfLUTTexture, 0);

    glViewport(0, 0, 512, 512);
    brdfShader.use();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderQuad();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    envTextures.push_back(brdfLUTTexture);

    return envTextures;
  };
  

  const unsigned int ViewerApplication::loadHdrTexture() const
{
    // pbr: load the HDR environment map
    // ---------------------------------
    stbi_set_flip_vertically_on_load(true);
    int width, height, nrComponents;
    float *data = stbi_loadf("img/hdr/papermill.hdr", &width, &height, &nrComponents, 0);
    unsigned int hdrTexture;
    if (data)
    {
        glGenTextures(1, &hdrTexture);
        glBindTexture(GL_TEXTURE_2D, hdrTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, data);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Failed to load HDR image." << std::endl;
    }

    return hdrTexture;
  };


unsigned int cubeVao = 0;
unsigned int cubeVbo = 0;

void ViewerApplication::renderCube()
{
    // initialize (if necessary)
    if (cubeVao == 0)
    {
        float vertices[] = {
            // back face
            -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
             1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
             1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
             1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
            -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
            -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
            // front face
            -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
             1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
             1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
             1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
            -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
            -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
            // left face
            -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
            -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
            -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
            -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
            -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
            -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
            // right face
             1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
             1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
             1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
             1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
             1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
             1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
            // bottom face
            -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
             1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
             1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
             1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
            -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
            -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
            // top face
            -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
             1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
             1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
             1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
            -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
            -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
        };
        glGenVertexArrays(1, &cubeVao);
        glGenBuffers(1, &cubeVbo);
        // fill buffer
        glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        // link vertex attributes
        glBindVertexArray(cubeVao);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }
    // render Cube
    glBindVertexArray(cubeVao);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
}

unsigned int quadVAO = 0;
unsigned int quadVBO;
void ViewerApplication::renderQuad()
{
    if (quadVAO == 0)
    {
        float quadVertices[] = {
            // positions        // texture Coords
            -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
             1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
             1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        // setup plane VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}