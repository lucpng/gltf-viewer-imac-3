#pragma once

#include "utils/GLFWHandle.hpp"
#include "utils/cameras.hpp"
#include "utils/filesystem.hpp"
#include "utils/shaders.hpp"
#include <tiny_gltf.h>

class ViewerApplication
{
public:
  ViewerApplication(const fs::path &appPath, uint32_t width, uint32_t height,
      const fs::path &gltfFile, const std::vector<float> &lookatArgs,
      const std::string &vertexShader, const std::string &fragmentShader,
      const fs::path &output);

  int run();

  bool loadGltfFile(tinygltf::Model &model);

private:
  // A range of indices in a vector containing Vertex Array Objects
  struct VaoRange
  {
    GLsizei begin; // Index of first element in vertexArrayObjects
    GLsizei count; // Number of elements in range
  };

  GLsizei m_nWindowWidth = 1920;
  GLsizei m_nWindowHeight = 1080;

  const fs::path m_AppPath;
  const std::string m_AppName;
  const fs::path m_ShadersRootPath;

  fs::path m_gltfFilePath;
  std::string m_vertexShader = "forward.vs.glsl";
  std::string m_fragmentShader = "pbr_ibl.fs.glsl";

  std::string m_vertexCubeMapShader = "cubemap.vs.glsl";
  std::string m_fragmentPanoToCubeMapShader = "pano_to_cubemap.fs.glsl";

  std::string m_fragmentIrradianceConvolutionShader = "irradiance_convolution.fs.glsl";

  std::string m_fragmentPrefilterShader = "prefilter.fs.glsl";

  std::string m_vertexBRDFShader = "brdf.vs.glsl";
  std::string m_fragmentBRDFShader = "brdf.fs.glsl";
  

  std::string m_vertexBackgroundShader = "background.vs.glsl";
  std::string m_fragmentBackgroundShader = "background.fs.glsl";
  bool m_hasUserCamera = false;
  Camera m_userCamera;

  fs::path m_OutputPath;
  
  // Order is important here, see comment below
  const std::string m_ImGuiIniFilename;
  // Last to be initialized, first to be destroyed:
  GLFWHandle m_GLFWHandle{int(m_nWindowWidth), int(m_nWindowHeight),
      "glTF Viewer",
      m_OutputPath.empty()}; // show the window only if m_OutputPath is empty
  /*
    ! THE ORDER OF DECLARATION OF MEMBER VARIABLES IS IMPORTANT !
    - m_ImGuiIniFilename.c_str() will be used by ImGUI in ImGui::Shutdown, which
    will be called in destructor of m_GLFWHandle. So we must declare
    m_ImGuiIniFilename before m_GLFWHandle so that m_ImGuiIniFilename
    destructor is called after.
    - m_GLFWHandle must be declared before the creation of any object managing
    OpenGL resources (e.g. GLProgram, GLShader) because it is responsible for
    the creation of a GLFW windows and thus a GL context which must exists
    before most of OpenGL function calls.
  */
  std::vector<GLuint> createBufferObjects( const tinygltf::Model &model);
  std::vector<GLuint> createVertexArrayObjects( const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> & meshIndexToVaoRange);
  std::vector<GLuint> createTextureObjects(const tinygltf::Model &model) const;
  std::vector<GLuint> setupCubeMaps(const GLProgram &panoToCubeMapShader, const GLProgram &irradianceShader,const GLProgram &prefilterShader, const GLProgram &brdfShader);
  void renderCube();
  void renderQuad();
  const unsigned int loadHdrTexture() const;
};

